from types import SimpleNamespace
from typing import NamedTuple, Tuple

import mock
import pytest
from fastapi.testclient import TestClient

from src.ska_ser_config_inspector import config
from src.ska_ser_config_inspector.emulator.mock_release import (
    get_mock_device_proxy,
    get_mock_release,
)


class TestContext(NamedTuple):
    client: TestClient
    mocked_release: mock.Mock

    def get_first_mocked_sub_chart_name(self) -> str:
        return list(self.mocked_release.sub_charts.keys())[0]

    def get_first_subchart_name_and_child_device(self) -> Tuple[str, str]:
        sub_chart_name, sub_chart = list(self.mocked_release.sub_charts.items())[0]
        device_name = list(sub_chart.devices.data.keys())[0]
        return sub_chart_name, device_name


class fxt_types(SimpleNamespace):
    test_context = TestContext


@pytest.fixture(name="mock_release", scope="session")
def fxt_mock_release():

    mock_release = get_mock_release(no_slashes=True)
    mock_proxy = get_mock_device_proxy()
    config.set_mock_device(mock_proxy)
    config.set_mock_release(mock_release)
    yield mock_release
    config.clear()


@pytest.fixture(name="test_context", scope="session")
def fxt_app(mock_release: mock.Mock) -> TestContext:
    from src.ska_ser_config_inspector.main import app

    client = TestClient(app)
    return TestContext(client, mock_release)
