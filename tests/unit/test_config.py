import os
from pathlib import Path

from ska_ser_config_inspector.config import load_configuration

__testdata_dir = "tests/data"


def test_load_configuration():
    expected_mapping = {
        "ska-csp-lmc-mid": "csp-lmc-test",
        "ska-mid-cbf-mcs": "cbfmcs-mid-test",
        "ska-tango-alarmhandler": "ska-tango-alarmhandler-test",
        "ska-tango-archiver": "ska-tango-archiver-test",
        "ska-tango-base": "ska-tango-base-test",
        "ska-tmc-mid": "ska-tmc-mid-test",
    }
    os.environ["CONFIG_FILE"] = Path(__testdata_dir, "cia-config.json").as_posix()
    cfg = load_configuration()
    assert cfg["chart_subsystem_mapping"] == expected_mapping
