import pytest
from assertpy import assert_that

from ..common import checking
from .conftest import fxt_types


def test_get_app_root(test_context: fxt_types.test_context):
    result = test_context.client.get("/")
    checking.check_httpx_response_get_app_root(result)


def test_get_chart_sub_chart(test_context: fxt_types.test_context):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    result = test_context.client.get(f"/{sub_chart}")
    checking.check_httpx_response_get_chart_sub_chart(result)


def test_get_chart_sub_chart_unsuccessfully(
    test_context: fxt_types.test_context,
):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    reverse_sub_chart_name = sub_chart[::-1]
    result = test_context.client.get(f"/{reverse_sub_chart_name}")
    assert result.is_client_error
    assert_that(result.status_code).is_equal_to(404)


def test_search_charts_fully(test_context: fxt_types.test_context):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    result = test_context.client.get("/search-charts", params={"patterns": sub_chart})
    checking.check_httpx_response_search_chart_sub_chart(result)


def test_search_charts_partially(test_context: fxt_types.test_context):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    _, _, pattern = sub_chart.partition("-")
    result = test_context.client.get("/search-charts", params={"patterns": pattern})
    checking.check_httpx_response_search_chart_sub_chart(result)


def test_search_charts_unsuccessfully(test_context: fxt_types.test_context):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    _, _, pattern = sub_chart.partition("-")
    reverse_pattern = pattern[::-1]
    result = test_context.client.get("/search-charts", params={"patterns": reverse_pattern})
    checking.check_httpx_response_search_chart_sub_chart_not_found(result)


def test_search_charts_all(test_context: fxt_types.test_context):
    result = test_context.client.get(
        "/search-charts",
    )
    checking.check_httpx_response_search_chart_sub_chart(result)


def test_get_device_from_sub_chart(test_context: fxt_types.test_context):
    sub_chart, device = test_context.get_first_subchart_name_and_child_device()
    result = test_context.client.get(f"/{sub_chart}/{device}")
    checking.check_httpx_response_get_device_from_sub_chart(result)


def test_get_device_from_sub_chart_unsuccessfully(
    test_context: fxt_types.test_context,
):
    sub_chart, device = test_context.get_first_subchart_name_and_child_device()
    reverse_device_name = device[::-1]
    result = test_context.client.get(f"/{sub_chart}/{reverse_device_name}")
    assert result.is_client_error
    assert_that(result.status_code).is_equal_to(404)


def test_search_device_fully(test_context: fxt_types.test_context):
    sub_chart, device = test_context.get_first_subchart_name_and_child_device()
    result = test_context.client.get(f"{sub_chart}/search-devices", params={"patterns": device})
    checking.check_httpx_response_search_devices_from_sub_chart(result)


def test_search_device_partially(test_context: fxt_types.test_context):
    sub_chart, device = test_context.get_first_subchart_name_and_child_device()
    _, _, pattern = device.partition("d_d_d")
    result = test_context.client.get(f"{sub_chart}/search-devices", params={"patterns": pattern})
    checking.check_httpx_response_search_devices_from_sub_chart(result)


def test_search_device_unsuccessfully(test_context: fxt_types.test_context):
    sub_chart, device = test_context.get_first_subchart_name_and_child_device()
    _, _, pattern = device.partition("d_d_d")
    reverse_pattern = pattern[::-1]
    result = test_context.client.get(
        f"{sub_chart}/search-devices", params={"patterns": reverse_pattern}
    )
    checking.check_httpx_response_search_chart_sub_chart_not_found(result)


def test_search_devices_all(test_context: fxt_types.test_context):
    sub_chart = test_context.get_first_mocked_sub_chart_name()
    result = test_context.client.get(
        f"{sub_chart}/search-devices",
    )
    checking.check_httpx_response_search_devices_from_sub_chart(result)


def test_ping(test_context: fxt_types.test_context):
    result = test_context.client.get("ping")
    checking.check_httpx_response_ping(result)


@pytest.mark.skip(reason="currently disable as skallop has an issue with this")
def test_search_all_devices_from_release(test_context: fxt_types.test_context):
    result = test_context.client.get("/search-devices")
    checking.check_httpx_response_search_devices_from_sub_chart(result)
