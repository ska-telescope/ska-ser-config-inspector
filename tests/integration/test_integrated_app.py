import urllib.parse
from typing import NamedTuple

import pytest
from assertpy import assert_that

from ..common import checking
from . import conftest


class Config(NamedTuple):
    default_chart_name: str = "ska-tango-base"
    chart_with_device: str = "ska-csp-lmc-mid"
    default_device_from_chart_with_device: str = """mid-csp/control/0"""

    @property
    def reversed_default_chart_name(self) -> str:
        return self.default_chart_name[::-1]

    @property
    def partial_default_chart_name(self) -> str:
        _, _, partial = self.default_chart_name.partition("ska-")
        return partial

    @property
    def reversed_partial_default_chart_name(self) -> str:
        partial = self.partial_default_chart_name
        reverse_pattern = partial[::-1]
        return reverse_pattern

    @property
    def reversed_default_device_name(self) -> str:
        return self.default_device_from_chart_with_device[::-1]

    @property
    def partial_default_device_name(self) -> str:
        _, _, partial = self.default_device_from_chart_with_device.partition("/0")
        return partial

    @property
    def reversed_partial_default_device_name(self) -> str:
        partial = self.partial_default_device_name
        reverse_pattern = partial[::-1]
        return reverse_pattern


@pytest.fixture(name="config")
def fxt_config() -> Config:
    return Config()


def test_ping(api: conftest.Api):
    result = api.get("ping")
    checking.check_requests_response_ping(result)


def test_get_app_root(api: conftest.Api):
    result = api.get("")
    checking.check_requests_response_get_app_root(result)


def test_get_chart_sub_chart(api: conftest.Api, config: Config):
    result = api.get(config.default_chart_name)
    checking.check_requests_response_get_chart_sub_chart(result)


def test_get_chart_sub_chart_unsuccessfully(api: conftest.Api, config: Config):
    reverse_sub_chart_name = config.reversed_default_chart_name
    result = api.get(f"/{reverse_sub_chart_name}")
    assert not result.ok
    assert_that(result.status_code).is_equal_to(404)


def test_search_charts_fully(api: conftest.Api, config: Config):
    sub_chart = config.default_chart_name
    result = api.get("/search-charts", params={"patterns": sub_chart})
    checking.check_requests_response_search_chart_sub_chart(result)


def test_search_charts_partially(api: conftest.Api, config: Config):
    pattern = config.partial_default_chart_name
    result = api.get("/search-charts", params={"patterns": pattern})
    checking.check_requests_response_search_chart_sub_chart(result)


def test_search_charts_unsuccessfully(api: conftest.Api, config: Config):
    reverse_pattern = config.reversed_partial_default_chart_name
    result = api.get("/search-charts", params={"patterns": reverse_pattern})
    checking.check_requests_response_search_chart_sub_chart_not_found(result)


def test_search_charts_all(api: conftest.Api):
    result = api.get(
        "/search-charts",
    )
    checking.check_requests_response_search_chart_sub_chart(result)


def double_encode(value: str):
    return urllib.parse.quote(urllib.parse.quote(value, safe=""), safe="")


def test_get_device_from_sub_chart(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    device = config.default_device_from_chart_with_device
    device_encoded = double_encode(device)
    result = api.get(f"{sub_chart}/{device_encoded}")
    checking.check_requests_response_get_device_from_sub_chart(result)


def test_get_device_from_sub_chart_unsuccessfully(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    device = config.reversed_default_device_name
    device_encoded = double_encode(device)
    result = api.get(f"/{sub_chart}/{device_encoded}")
    assert not result.ok
    assert_that(result.status_code).is_equal_to(404)


def test_search_device_fully(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    device = config.default_device_from_chart_with_device
    result = api.get(f"{sub_chart}/search-devices", params={"patterns": device})
    checking.check_requests_response_search_devices_from_sub_chart(result)


def test_search_device_partially(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    pattern = config.partial_default_device_name
    result = api.get(f"{sub_chart}/search-devices", params={"patterns": pattern})
    checking.check_requests_response_search_devices_from_sub_chart(result)


def test_search_device_unsuccessfully(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    reverse_pattern = config.reversed_partial_default_chart_name
    result = api.get(f"{sub_chart}/search-devices", params={"patterns": reverse_pattern})
    checking.check_requests_response_search_chart_sub_chart_not_found(result)


def test_search_devices_all(api: conftest.Api, config: Config):
    sub_chart = config.chart_with_device
    result = api.get(
        f"{sub_chart}/search-devices",
    )
    checking.check_requests_response_search_devices_from_sub_chart(result)
