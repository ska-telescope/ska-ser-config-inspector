import logging
import os
from typing import Union

import pytest
import requests
from kubernetes import client, config
from kubernetes.client.models.v1_service import V1Service
from kubernetes.client.models.v1_service_spec import V1ServiceSpec
from kubernetes.client.models.v1_service_status import V1ServiceStatus

logger = logging.getLogger(__name__)


class Api:

    app_endpoint = "config-inspector"
    svc_name = "config-inspector"

    def __init__(self) -> None:
        assert (
            namespace := os.getenv("KUBE_NAMESPACE")
        ), "Unable to load API object; did you set KUBE_NAMESPACE?"
        if "USE_SVC_IP" in os.environ:
            self._base_url = load_svc_url(namespace, self.svc_name)
        else:
            assert (
                host := os.getenv("KUBE_HOST")
            ), "Unable to load API object; did you set KUBE_HOST?"
            self._base_url = f"http://{host}/{namespace}/{self.app_endpoint}"

    def _url(self, endpoint: str) -> str:
        endpoint = endpoint.lstrip("/")
        return f"{self._base_url}/{endpoint}"

    def get(self, endpoint: str, params: Union[None, dict] = None) -> requests.Response:
        url = self._url(endpoint)
        logger.debug(f"GET: url: {url}, params: {params}")
        return requests.get(url, params, timeout=30)


def load_svc_url(namespace: str, svc_name: str):
    config.load_kube_config()
    client_v1 = client.CoreV1Api()
    svc: V1Service = client_v1.read_namespaced_service(
        name=svc_name, namespace=namespace, pretty="true"
    )
    status = V1ServiceStatus(svc.status).to_dict()
    ip_address = status["conditions"]["load_balancer"]["ingress"][0]["ip"]
    spec = V1ServiceSpec(svc.spec).to_dict()
    port = spec["allocate_load_balancer_node_ports"]["ports"][0]["port"]
    return f"http://{ip_address}:{port}"


@pytest.fixture(name="api")
def fxt_api():
    return Api()
