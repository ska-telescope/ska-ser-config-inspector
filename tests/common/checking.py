import json
from typing import cast

import httpx
import requests
from assertpy import assert_that


def check_requests_response_get_app_root(result: requests.Response):
    assert result.ok
    _check_response_get_app_root(json.loads(result.text))


def check_httpx_response_get_app_root(result: httpx.Response):
    assert result.is_success
    _check_response_get_app_root(json.loads(result.text))


def _check_response_get_app_root(result_data: dict):
    assert_that(result_data).contains_key("chart", "version", "sub_charts")
    sub_charts = result_data["sub_charts"]
    assert_that(sub_charts).is_type_of(list)


def check_requests_response_search_chart_sub_chart_not_found(result: requests.Response):
    assert result.ok
    _check_response_search_chart_sub_chart_not_found(json.loads(result.text))


def check_httpx_response_search_chart_sub_chart_not_found(result: httpx.Response):
    assert result.is_success
    _check_response_search_chart_sub_chart_not_found(json.loads(result.text))


def _check_response_search_chart_sub_chart_not_found(result_data: dict):
    assert_that(result_data).is_instance_of(list)
    assert_that(result_data).is_empty()


def check_requests_response_search_chart_sub_chart(result: requests.Response):
    assert result.ok
    _check_response_search_chart_sub_chart(json.loads(result.text))


def check_httpx_response_search_chart_sub_chart(result: httpx.Response):
    assert result.is_success
    _check_response_search_chart_sub_chart(json.loads(result.text))


def _check_response_search_chart_sub_chart(result_data: dict):
    assert_that(result_data).is_instance_of(list)
    chart_response = cast(list, result_data).pop()
    _check_sub_chart_from_response(chart_response)


def _check_sub_chart_from_response(result_data: dict):
    assert_that(result_data).contains_key("chart", "version", "health", "devices", "deployments")
    devices = result_data["devices"]
    if devices:
        assert_that(devices).is_type_of(dict)
        assert_that(devices).contains_key("overall_health", "states", "names")
    deployments = result_data["deployments"]
    assert_that(deployments).is_type_of(dict)


def check_requests_response_get_chart_sub_chart(result: requests.Response):
    assert result.ok
    _check_sub_chart_from_response(json.loads(result.text))


def check_httpx_response_get_chart_sub_chart(result: httpx.Response):
    assert result.is_success
    _check_sub_chart_from_response(json.loads(result.text))


def check_requests_response_ping(result: requests.Response):
    assert result.ok
    _check_response_ping(json.loads(result.text))


def check_httpx_response_ping(result: httpx.Response):
    assert result.is_success
    _check_response_ping(json.loads(result.text))


def _check_response_ping(result_data: dict):
    assert_that(result_data).contains_key(
        "result",
        "time",
    )
    result_status = result_data["result"]
    assert_that(result_status).is_equal_to("ok")
    result_time = result_data["time"]
    assert_that(result_time).is_type_of(str)


def _check_response_device_from_subchart(result_data: dict):
    assert_that(result_data).contains_key(
        "name",
        "deployment_status",
        "device_properties",
        "state",
        "device_attributes",
    )


def check_requests_response_get_device_from_sub_chart(result: requests.Response):
    assert result.ok
    _check_response_device_from_subchart(json.loads(result.text))


def check_httpx_response_get_device_from_sub_chart(result: httpx.Response):
    assert result.is_success
    _check_response_device_from_subchart(json.loads(result.text))


def check_requests_response_search_devices_from_sub_chart(result: requests.Response):
    assert result.ok
    _check_requests_response_search_devices_from_sub_chart(json.loads(result.text))


def check_httpx_response_search_devices_from_sub_chart(result: httpx.Response):
    assert result.is_success
    _check_requests_response_search_devices_from_sub_chart(json.loads(result.text))


def _check_requests_response_search_devices_from_sub_chart(result_data: dict):
    assert_that(result_data).is_instance_of(list)
    device_response = cast(list, result_data).pop()
    _check_response_device_from_subchart(device_response)
