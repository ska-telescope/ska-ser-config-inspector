FROM thehale/python-poetry:1.8.2-py3.10-slim

WORKDIR /app

COPY . /app/

RUN poetry install

CMD ["poetry", "run", "uvicorn", "ska_ser_config_inspector.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "80"]
