{{/*
Expand the name of the chart.
*/}}
{{- define "config-inspector.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "config-inspector.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "config-inspector.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "config-inspector.labels" -}}
helm.sh/chart: {{ include "config-inspector.chart" . }}
{{ include "config-inspector.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
app: {{ .Values.global.labels.app }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "config-inspector.selectorLabels" -}}
app.kubernetes.io/name: {{ include "config-inspector.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "config-inspector.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "config-inspector.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}


{{/*
Create app image url
*/}}
{{- define "config-inspector.image" -}}
{{- .Values.image.repository }}/{{ .Values.image.name }}:{{ .Values.image.tag | default .Chart.AppVersion }}
{{- end }}

{{/*
CIA configmap name
*/}}
{{- define "config-inspector.configName" -}}
cia-config-{{ .Release.Name }}
{{- end }}


{{/*
CIA configmap file path
*/}}
{{- define "config-inspector.configPath" -}}
{{ .Values.configFile.directory }}/{{ .Values.configFile.filename }}
{{- end }}

{{- define "config-inspector.annotations" -}}
{{- with .Values.global.annotations }}
{{- toYaml . }}
{{- end }}
timestamp: {{ now | quote }}
{{- end }}
