=================
API Documentation
=================

The Config Inspector API provides methods for inspecting the runtime configuration of a system running in Kubernetes.
It also provides methods for inspecting any deployed Tango devices.

Retrieve Release Information
============================

**Method: /**

The API root provides information about the main chart and summary info about it's constituent sub-charts.

Example Usage
-------------

``curl -s http://192.168.49.2:30845 | jq``

.. code-block:: javascript

    {
        "chart": "config-inspector-umbrella-chart",
        "version": "0.1.0",
        "sub_charts": [
            {
                "chart": "config-inspector",
                "version": "0.1.0"
            },
            {
                "chart": "ska-tango-base",
                "version": "0.3.5"
            },
            {
                "chart": "ska-tango-util",
                "version": "0.3.5"
            },
            {
                "chart": "ska-mid-cbf",
                "version": "0.2.9"
            },
            {
                "chart": "ska-csp-lmc-mid",
                "version": "0.11.5"
            }
        ]
    }

Search for charts
=================

**Method: /search-charts?patterns={patterns}**

This API method provides a way to search for sub-charts based on a list of regex patterns.
Each chart that matches one or more of the patterns is retrieved.

.. warning::

    Chart devices will not be loaded if an alias is used.

Parameters
----------

1. ``patterns``: a comma-separated list of regex patterns used to match charts. If no arguments are given the server will return all the charts.

Example Usage
-------------

``curl -s http://192.168.49.2:30845/search-charts?patterns=ska-tango-base | jq``

.. code-block:: javascript

    [
        {
            "chart": "ska-tango-base",
            "version": "0.3.5",
            "health": "READY",
            "deployments": {
                "overall_health": "UNKNOWN",
                "instances": {}
            },
            "devices": null
        }
    ]

Retrieve specific chart details
===============================

**Method: /{chart_name}**

Retrieve detailed information about a specific chart.

.. warning::

    Chart devices will not be loaded if an alias is used.

Parameters
----------

1. ``chart_name``: The name of the sub-chart to retrieve information from.

Example Usage
-------------

``curl -s http://192.168.49.2:30845/ska-csp-lmc-mid | jq``

.. code-block:: javascript

    {
        "chart": "ska-csp-lmc-mid",
        "version": "0.11.5",
        "health": "READY",
        "deployments": {
            "overall_health": "UNKNOWN",
            "instances": {}
        },
        "devices": {
            "overall_health": "READY",
            "states": {
                "mid-csp/control/0": "DISABLE",
                "mid-csp/subarray/01": "OFF",
                "mid-csp/subarray/02": "OFF",
                "mid-csp/subarray/03": "OFF",
                "sys/tg_test/1": "RUNNING",
                "sys/database/2": "ON"
            },
            "names": [
                "mid-csp/control/0",
                "mid-csp/subarray/01",
                "mid-csp/subarray/02",
                "mid-csp/subarray/03",
                "sys/database/2",
                "sys/tg_test/1"
            ]
        }
    }

Search for Tango devices
========================

**Method: /search-devices?patterns={patterns}**

This API method provides a way to search for Tango devices across all charts based on a list of regex patterns.
Each Tango devices that matches one or more of the patterns is retrieved.

.. warning::

   1. Chart devices will not be loaded if an alias is used.

Parameters
----------

1. ``patterns``: a comma-separated list of regex patterns used to match Tango devices. If no arguments are given the server will return all the Tango devices. You cannot include a forward slash in the patterns.

Search for a sub-chart's Tango devices
======================================

**Method: /{chart_name}/search-devices?patterns={patterns}**

This API method provides a way to search for Tango devices within a specific sub-chart based on a list of regex patterns.
Each Tango devices that matches one or more of the patterns is retrieved.

.. warning::

    Chart devices will not be loaded if an alias is used.

Parameters
----------

1. ``chart_name``: The name of the sub-chart to search in.
2. ``patterns``: a comma-separated list of regex patterns used to match Tango devices. If no arguments are provided the server will return all the chart's Tango devices.

Example Usage
-------------

``curl -s http://192.168.49.2:31374/ska-mid-cbf/search-devices?patterns=controller | jq``

.. code-block:: javascript

    [
        {
            "name": "mid_csp_cbf/sub_elt/controller",
            "deployment_status": "Running",
            "device_properties": {
                "CbfSubarray": [
                    "mid_csp_cbf/sub_elt/subarray_01",
                    "mid_csp_cbf/sub_elt/subarray_02",
                    "mid_csp_cbf/sub_elt/subarray_03"
                ],
                "FSP": [
                    "mid_csp_cbf/fsp/01",
                    "mid_csp_cbf/fsp/02",
                    "mid_csp_cbf/fsp/03",
                    "mid_csp_cbf/fsp/04"
                ],
                "MaxCapabilities": [
                    "VCC:4",
                    "FSP:4",
                    "Subarray:1"
                ],
                "VCC": [
                    "mid_csp_cbf/vcc/001",
                    "mid_csp_cbf/vcc/002",
                    "mid_csp_cbf/vcc/003",
                    "mid_csp_cbf/vcc/004"
                ],
                "TalonLRU": [
                    "mid_csp_cbf/talon_lru/001"
                ],
                "TalonDxConfigPath": [
                    "mnt/talondx-config"
                ],
                "polled_attr": [
                    "reportfspstate",
                    "3000",
                    "reportvccadminmode",
                    "3000",
                    "reportvcchealthstate",
                    "3000",
                    "receptortovcc",
                    "3000",
                    "reportfspsubarraymembership",
                    "3000",
                    "reportfsphealthstate",
                    "3000",
                    "healthstate",
                    "3000",
                    "subarrayconfigid",
                    "3000",
                    "reportfspadminmode",
                    "3000",
                    "commandprogress",
                    "3000",
                    "reportsubarrayhealthstate",
                    "3000",
                    "reportvccstate",
                    "3000",
                    "reportsubarrayadminmode",
                    "3000",
                    "vcctoreceptor",
                    "3000",
                    "reportsubarraystate",
                    "3000"
                ]
            },
            "device_attributes": [
                "buildState",
                "versionId",
                "loggingLevel",
                "loggingTargets",
                "healthState",
                "adminMode",
                "controlMode",
                "simulationMode",
                "testMode",
                "elementLoggerAddress",
                "elementAlarmAddress",
                "elementTelStateAddress",
                "elementDatabaseAddress",
                "maxCapabilities",
                "availableCapabilities",
                "commandProgress",
                "receptorToVcc",
                "vccToReceptor",
                "subarrayconfigID",
                "reportVCCState",
                "reportVCCHealthState",
                "reportVCCAdminMode",
                "reportVCCSubarrayMembership",
                "reportFSPState",
                "reportFSPHealthState",
                "reportFSPAdminMode",
                "reportFSPSubarrayMembership",
                "frequencyOffsetK",
                "frequencyOffsetDeltaF",
                "reportSubarrayState",
                "reportSubarrayHealthState",
                "reportSubarrayAdminMode",
                "State",
                "Status"
            ],
            "state": "OFF",
            "chart": "ska-mid-cbf"
        }
    ]


Get a specific Tango device from a sub-chart
============================================

**Method: /{chart_name}/{device_name}**

This API method provides a way to get details of a specific for Tango device within a specific sub-chart.

Parameters
----------

1. ``chart_name``: The name of the sub-chart.
2. ``device_name``: The name of the Tango device to display. This parameter **must** be URL encoded twice.

.. warning::

   1. Chart devices will not be loaded if an alias is used.

Example Usage
-------------

``curl -s http://192.168.49.2:31374/ska-csp-lmc-mid/mid-csp%252Fcontrol%252F0 | jq``

.. code-block:: javascript

    {
        "name": "mid-csp/control/0",
        "deployment_status": "Running",
        "device_properties": {
            "ConnectionTimeout": [
                "900"
            ],
            "PingConnectionTime": [
                "5"
            ],
            "DefaultCommandTimeout": [
                "5"
            ],
            "CspCbf": [
                "mid_csp_cbf/sub_elt/controller"
            ],
            "CspVccCapability": [
                "mid-csp/capability-vcc/0"
            ],
            "CspFspCapability": [
                "mid-csp/capability-fsp/0"
            ],
            "CspSubarrays": [
                "mid-csp/subarray/01",
                "mid-csp/subarray/02",
                "mid-csp/subarray/03"
            ],
            "MaxCapabilities": [
                "VCC:4",
                "SearchBeams:1500",
                "TimingBeams:16",
                "VlbiBeams:20"
            ],
            "SkaLevel": [
                "1"
            ]
        },
        "device_attributes": [
            "buildState",
            "versionId",
            "loggingLevel",
            "loggingTargets",
            "healthState",
            "adminMode",
            "controlMode",
            "simulationMode",
            "testMode",
            "longRunningCommandsInQueue",
            "longRunningCommandIDsInQueue",
            "longRunningCommandStatus",
            "longRunningCommandProgress",
            "longRunningCommandResult",
            "elementLoggerAddress",
            "elementAlarmAddress",
            "elementTelStateAddress",
            "elementDatabaseAddress",
            "maxCapabilities",
            "availableCapabilities",
            "commandResult",
            "isCommunicating",
            "commandTimeout",
            "cspCbfState",
            "cspPssState",
            "cspPstBeamsState",
            "cspCbfHealthState",
            "cspPssHealthState",
            "cspPstBeamsHealthState",
            "cbfControllerAddress",
            "pssControllerAddress",
            "pstBeamsAddresses",
            "cspCbfAdminMode",
            "cspPssAdminMode",
            "cspPstBeamsAdminMode",
            "numOfDevCompletedTask",
            "cbfVersion",
            "pssVersion",
            "pstVersion",
            "jsonComponentVersions",
            "testAlarm",
            "cbfSimulationMode",
            "commandResultName",
            "commandResultCode",
            "receptorsList",
            "receptorMembership",
            "unassignedReceptorIDs",
            "cspVccCapabilityAddress",
            "cspFspCapabilityAddress",
            "vccAddresses",
            "fspAddresses",
            "dishVccConfig",
            "sourceDishVccConfig",
            "alarmDishIdCfg",
            "State",
            "Status"
        ],
        "state": "DISABLE",
        "chart": "ska-csp-lmc-mid"
    }

Check the API health
====================

**Method: /ping**

This API method provides does a basic health check.

Example Usage
-------------

``curl -s http://192.168.49.2:31374/ska-mid-cbf/ping | jq``

.. code-block:: javascript

    {
        "result": "ok",
        "time": "2024-03-19T08:27:34.852493"
    }
