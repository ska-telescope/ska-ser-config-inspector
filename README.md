# Config Inspector API

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-configuration-inspector-api/badge/?version=latest)](https://ska-telescope-ska-configuration-inspector-api.readthedocs.io/en/latest/?badge=latest)

## Introduction

The config inspector API provides methods for extracting Kubernetes and Tango configuration of a deployed system.

## Known Issues/Gotchas

1. Loading Tango device info will fail if your chart uses aliases for its dependencies.
2. The API method which uses a Tango device name triplet in its path requires you to double URL encode the device name triplet. The corresponding Python method is: `get_device_from_a_given_sub_chart`.

## K8s Deployment Steps

### Recommended prerequisites

Use the SKA Deploy Minikube repository.

Set your `PrivateRules.mk` as follows:

```
IMAGE_REPOSITORY = registry.gitlab.com/ska-telescope/ska-ser-config-inspector
K8S_CHART_PARAMS = --set image.tag=latest --set image.repository=$(IMAGE_REPOSITORY) --set image.pullPolicy=Always
KUBE_NAMESPACE = yournamespacename ## Make good choices here, defaults to ska-ser-config-inspector
KUBE_HOST = yourminikubeipaddress ## Get this from $(minikube ip)
```

While testing, the following can also be set in your `PrivateRules.mk`:

```
# This uses mock releases and tango devices instead of the real thing.
MOCKMODE=True
# This starts up the container, but does not run the app.
# Instead, it runs `sleep infinity`.
# This allows you to shell into the container and debug any issues.
DEVMODE=True
```

### Build your image

Make sure docker builds are pushed to the minikube local registry:

```
eval $(minikube docker-env)
```

Build the image:

```
make oci-build-all
```

### Install the charts and use the documentation

Run

```
make k8s-install-chart && make k8s-wait
```

Open your browser at http://$KUBE_HOST/$KUBE_NAMESPACE/config-inspector/docs.
Alternatively, the LoadBalancer service URL can be obtained with: `minikube service --namespace ska-ser-config-inspector config-inspector --url`.

If you're lucky, this can all be done with

```
make run
```
