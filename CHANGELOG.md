# Version History

## Unreleased

## 0.2.3

* [AT-2001] - Update pyproject.toml with new source for pypi and remove deprecated CAR URL.
* [AT-1945] - Update CIA to always redeploy so that it picks up all chart version changes.
* [AT-1945] - Add openapi schema.

## 0.2.2

* [AT-1945] - Make chartinfo configmap shareable.

## 0.2.1

* [AT-1945] - Updates to get CIA working with SUT.
  * Give permissions on deviceserver CRDs.
  * Use new version of skallop which supports deviceserver CRDs.
  * Switch to using subsystem labels for finding chart devices.
  * Add a configmap containing chart -> subsystem mappings.
  * Update versions of SUT in umbrella chart

## 0.2.0

* [AT-1945] - Update dependencies and refactor repo to comply with SKAO standards.
