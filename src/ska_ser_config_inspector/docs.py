description = """Get the configurational status of a currently deployed product in terms of its sub
    configurational items and their corresponding health"""

tags_metadata = [
    {
        "name": "Control",
    },
    {
        "name": "Charts and release data",
    },
    {
        "name": "Tango Devices and their deployment status",
    },
]

docs = {
    "title": "config-inspector",
    "description": description,
    "version": "0.2.3",
    "openapi_tags": tags_metadata,
}
