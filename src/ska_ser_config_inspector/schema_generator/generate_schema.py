import argparse
import json

from fastapi.openapi.utils import get_openapi

from ska_ser_config_inspector.main import app


def generate_schema(output_file: str):
    with open(output_file, "w", encoding="utf-8") as f:
        openapi_schema = get_openapi(
            title=app.title,
            version=app.version,
            openapi_version=app.openapi_version,
            summary=app.summary,
            description=app.description,
            routes=app.routes,
            servers=app.servers,
            terms_of_service=app.terms_of_service,
            contact=app.contact,
            license_info=app.license_info,
        )
        json.dump(openapi_schema, f, indent=4)


def main():
    parser = argparse.ArgumentParser(
        "generate_schema",
    )
    parser.add_argument("schema", help="The file to write the schema to.", type=str)
    args = parser.parse_args()
    generate_schema(args.schema)


if __name__ == "__main__":
    main()
