import re
from typing import Any, TypeVar
from unittest.mock import Mock

from ska_ser_skallop.mvp_control.infra_mon.helm import Chart, PodSet
from ska_ser_skallop.mvp_control.infra_mon.tango_infra import Device, Devices, DevicesChart

from ska_ser_config_inspector import DeviceProxy, TangoBasedRelease

T = TypeVar("T")


def get_mock_deployment_instance():
    mock_deployment = Mock(PodSet)
    mock_deployment.resource_type = "Statefulset"
    mock_deployment.phase = "Running"
    return mock_deployment


def get_mock_deployment_instances(quantity: int):
    for _ in range(quantity):
        yield get_mock_deployment_instance()


def _get_id_from_mock(mock_name: str) -> str:
    pattern = re.compile(r"(?<=id\=')(.*)(?=')")
    result = pattern.findall(mock_name)
    assert result, "Unable to get id from given input using regex"
    return result[0]


def get_mock_device_instance(chart_name: str, no_slashes: bool = False):
    device = Mock(Device)
    mock_id = _get_id_from_mock(str(device))
    device.name = f"d_d_d{mock_id}" if no_slashes else f"d/d/d{mock_id}"
    device.device_class = "deviceClass"
    device.device_server = f"deviceServer{mock_id}"
    device.properties = {"property1": "value1", "property2": "value2"}
    device.chart = chart_name
    device.get_device_deployment_status.side_effect = [
        "Running",
        "Pending",
        "Succeeded",
        "Failed",
        "Unknown",
    ]
    return device


def get_mock_device_instances(chart_name: str, quantity: int, no_slashes: bool = False):
    for _ in range(quantity):
        yield get_mock_device_instance(chart_name, no_slashes)


def set_dict(input_dict: dict[str, Any], value: T) -> dict[str, T]:
    return {key: value for key in input_dict.keys()}


def get_mock_devices(chart_name: str, no_slashes: bool = False):
    devices = Mock(Devices)
    device_instances = list(get_mock_device_instances(chart_name, 2, no_slashes))
    devices.data = {device.name: device for device in device_instances}
    devices.names = list(devices.data.keys())
    devices.read_attributes.return_value = set_dict(devices.data, "ON")
    devices.ping.return_value = set_dict(devices.data, 100)
    devices.command_inout.return_value = set_dict(devices.data, "OK")
    devices.states = set_dict(devices.data, "ON")
    devices.write_attributes.return_value = set_dict(devices.data, "OK")
    devices.not_ready = Mock(Devices)
    devices.not_ready.data = devices.data
    devices.in_error = Mock(Devices)
    devices.in_error.data = devices.data
    devices.ready = Mock(Devices)
    devices.ready.data = devices.data
    devices.items.side_effect = devices.data.items
    return devices


def get_mock_chart_instance(is_devices_chart: bool = False, no_slashes: bool = False):
    mock_chart = Mock(Chart) if not is_devices_chart else Mock(DevicesChart)
    mock_id = _get_id_from_mock(str(mock_chart))
    mock_chart.name = f"chart-{mock_id}"
    mock_chart.version = "1.0.0"
    mock_chart.health = "READY"
    mock_chart.deployments_health = "READY"
    mock_chart.deployments = {
        str(deployment): deployment for deployment in get_mock_deployment_instances(2)
    }
    if is_devices_chart:
        mock_chart.devices_health = "READY"
        mock_chart.devices = get_mock_devices(mock_chart.name, no_slashes)
        mock_chart.device_states = mock_chart.devices.states
    return mock_chart


def get_mock_chart_instances(quantity: int, no_slashes: bool = False):
    flip = True
    for _ in range(quantity):
        is_devices_chart = flip
        yield get_mock_chart_instance(is_devices_chart, no_slashes=no_slashes)
        flip = not flip


def get_mock_release(no_slashes: bool = False) -> Mock:
    mock_release = Mock(TangoBasedRelease)
    mock_release.sub_charts = {
        mock_chart.name: mock_chart for mock_chart in get_mock_chart_instances(2, no_slashes)
    }
    mock_release.chart = get_mock_chart_instance(no_slashes=no_slashes)
    devices = [
        chart.devices.data
        for chart in mock_release.sub_charts.values()
        if isinstance(chart, DevicesChart)
    ][0]
    mock_release.devices = devices
    return mock_release


def get_mock_device_proxy() -> DeviceProxy:
    proxy = Mock(DeviceProxy)
    proxy.get_attribute_list.return_value = ["attr1", "attr2"]
    return proxy
