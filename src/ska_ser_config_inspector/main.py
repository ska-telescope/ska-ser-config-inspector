import re
import urllib.parse
from datetime import datetime
from typing import cast

from fastapi import FastAPI, HTTPException

from . import Chart, Device, DevicesChart, TangoBasedRelease, config, schema
from .docs import docs

app = FastAPI(**docs)

_release: TangoBasedRelease = None


def release() -> TangoBasedRelease:
    global _release  # pylint: disable=global-statement
    if _release is None:
        _release = config.get_release()
    return _release


# control


@app.get("/ping", response_model=schema.PingResponse, tags=["control"])
def ping_server_and_get_current_time_on_server():
    """Get a response from server to indicate it is available.

    :return: the current time on the server.
    :rtype: *schema.PingResponse*
    """
    return schema.PingResponse(result="ok", time=datetime.now())


# Charts and release data


def _get_chart_data(chart: Chart) -> schema.ChartResponse:
    devices = None
    if isinstance(chart, DevicesChart):
        devices = schema.Devices(
            overall_health=chart.devices_health,
            states=chart.device_states,
            names=list(chart.devices.names),
        )
    deployment_instances = {
        name: schema.DeploymentInstance(
            resource_type=deployment.resource_type, phase=deployment.phase
        )
        for name, deployment in chart.deployments.items()
    }
    return schema.ChartResponse(
        chart=chart.name,
        version=chart.version,
        health=chart.health,
        deployments=schema.Deployments(
            overall_health=chart.deployments_health,
            instances=deployment_instances,
        ),
        devices=devices,
    )


def _get_chart_summary(chart: Chart) -> schema.ChartSummary:
    return schema.ChartSummary(chart=chart.name, version=chart.version)


@app.get(
    "/",
    response_model=schema.ReleaseResponse,
    tags=["Charts and release data"],
)
def get_release() -> schema.ReleaseResponse:
    """Get main chart (Release) info and summary info for it's constituent sub charts.

    :return: data about the current release
    :rtype: *schema.ReleaseResponse*
    """
    return schema.ReleaseResponse(
        chart=release().chart.name,
        version=release().chart.version,
        sub_charts=[_get_chart_summary(chart) for chart in release().sub_charts.values()],
    )


@app.get(
    "/search-charts",
    response_model=set[schema.ChartResponse],
    tags=["Charts and release data"],
)
def search_for_sub_charts(patterns: str = "") -> set[schema.ChartResponse]:
    """Search the release's sub charts for any charts whose name fit the given pattern/s.

    The pattern is a regex pattern and can be multiple instances if separated by ", ".

    **Note**: if no arguments are given the server will return all the sub charts.

    :param patterns: the subcharts matching the pattern/s
        defaults to "" indicating all subcharts must be returned.
    :rtype patterns: str
    :return: a set of charts containing detail information for each chart.
    :rtype: set[schema.ChartResponse]
    """
    # first do the long running command
    pattern_list = patterns.split(", ") if patterns else []
    sub_charts = release().sub_charts
    # then do the filtering
    if not pattern_list:
        return {_get_chart_data(chart) for chart in sub_charts.values()}
    response: set[schema.ChartResponse] = set()
    for pattern in pattern_list:
        for chart_name, chart in sub_charts.items():
            regex_pattern = re.compile(pattern)
            if regex_pattern.findall(chart_name):
                response.add(_get_chart_data(chart))
    return response


def _create_device_response_from_device(device_name: str, device: Device, chart: DevicesChart):
    if not (device_state := chart.device_states.get(device_name)):
        device_state = "unknown"
        device_attributes = []
    else:
        device_proxy = config.get_device_proxy(device.name)
        device_attributes = list(device_proxy.get_attribute_list())
    return schema.DeviceResponse(
        name=device.name,
        deployment_status=device.get_device_deployment_status(),
        device_properties=device.properties,
        state=device_state,
        device_attributes=device_attributes,  # type: ignore
        chart=chart.name,
    )


def _get_device_from_a_given_sub_chart(chart: Chart, device_name: str):
    if isinstance(chart, DevicesChart):
        if device := chart.devices.data.get(device_name):
            return _create_device_response_from_device(device_name, device, chart)
    raise HTTPException(status_code=404, detail=f"device not found: {device_name}")


@app.get(
    "/{chart_name}/search-devices",
    response_model=set[schema.DeviceResponse],
    tags=["Tango Devices and their deployment status"],
)
def search_sub_chart_for_devices(
    chart_name: str, patterns: str = ""
) -> set[schema.DeviceResponse]:
    """Search a given subchart for any devices whose name fit the given pattern/s.

    The pattern is a regex pattern and can be multiple instances if separated by ", ".

    **Note**: if no arguments are given the server will return all the devices.

    :param chart_name: the sub chart that should contain the device
    :type chart_name: str
    :param patterns: the devices matching the pattern/s
        defaults to "" indicating all devices must be returned.
    :rtype patterns: str, optional
    :return: the information about the searched devices
        (None if not found and all if no pattern given)
    :rtype: set[schema.DeviceResponse]
    """
    responses: set[schema.DeviceResponse] = set()
    chart = release().sub_charts.get(chart_name)
    if isinstance(chart, DevicesChart):
        for device_name, device in chart.devices.items():
            device_name = cast(str, device_name)
            device = cast(Device, device)
            pattern_list = patterns.split(", ") if patterns else []
            if pattern_list:
                for pattern in pattern_list:
                    regex_pattern = re.compile(pattern)
                    if regex_pattern.findall(device_name):
                        responses.add(
                            _create_device_response_from_device(device_name, device, chart)
                        )
            # if no arg given we return all the devices
            else:
                responses.add(_create_device_response_from_device(device_name, device, chart))
    return responses


@app.get(
    "/{chart_name}/{device_name}",
    response_model=schema.DeviceResponse,
    tags=["Tango Devices and their deployment status"],
)
def get_device_from_a_given_sub_chart(chart_name: str, device_name: str):
    """Get a know device from a given subchart as defined in the path structure.

    :param chart_name: the sub chart that contains the device
    :type chart_name: str
    :param device_name: the device name to be returned
    :type device_name: str
    :raises HTTPException: if the chart could not be found
    :return: the information about the given device (None if not found)
    :rtype: *Union[schema.DeviceResponse, None]
    """
    if chart := release().sub_charts.get(chart_name):
        device_name = urllib.parse.unquote(device_name)
        return _get_device_from_a_given_sub_chart(chart, device_name)
    raise HTTPException(status_code=404, detail=f"chart not found: {chart_name}")


@app.get(
    "/search-devices",
    response_model=set[schema.DeviceResponse],
    tags=["Tango Devices and their deployment status"],
)
def search_release_for_devices(
    patterns: str = "",
) -> set[schema.DeviceResponse]:
    """
    Search a release for any devices whose name fit the given pattern/s.

    :param patterns: the devices matching the pattern/s
        defaults to "", indicating **all** devices must be returned.
    :type patterns: str
    :return: the information about the searched devices
        (None if not found and all if no pattern given)
    :rtype: set[schema.DeviceResponse]
    """
    responses: set[schema.DeviceResponse] = set()
    for device_name, device in release().devices.items():
        device_name = cast(str, device_name)
        device = cast(Device, device)
        pattern_list = patterns.split(", ") if patterns else []
        chart_name = device.chart
        chart = release().sub_charts.get(chart_name)
        assert chart, "unknown chart indicated by the given device"
        if pattern_list:
            for pattern in pattern_list:
                regex_pattern = re.compile(pattern)
                if regex_pattern.findall(device_name):
                    responses.add(
                        _create_device_response_from_device(
                            device_name, device, cast(DevicesChart, chart)
                        )
                    )
        # if no arg given we return all the devices
        else:
            responses.add(
                _create_device_response_from_device(device_name, device, cast(DevicesChart, chart))
            )
    return responses


@app.get(
    "/{chart_name}",
    response_model=schema.ChartResponse,
    tags=["Charts and release data"],
)
def get_sub_chart(chart_name: str) -> schema.ChartResponse:
    """
    Get detail information about a given chart.

    :param chart_name: the sub chart requiring more information
    :type chart_name: str
    :raises HTTPException: if the chart is not found
    :return: detail information about a given chart
    :rtype: schema.ChartResponse
    """
    if chart := release().sub_charts.get(chart_name):
        return _get_chart_data(chart)
    raise HTTPException(status_code=404, detail="Sub chart not found")
