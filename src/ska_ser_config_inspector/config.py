import json
import os
from typing import Dict, TypedDict, Union, cast

from mock import Mock
from ska_ser_skallop.connectors.configuration import get_device_proxy as skallop_get_device_proxy
from ska_ser_skallop.connectors.tangofactory import get_tango_factory
from ska_ser_skallop.mvp_control.infra_mon.configuration import (
    get_mvp_release,
    set_factory_provider,
)
from ska_ser_skallop.mvp_control.infra_mon.helm import default_chart_subsystem_mapping

from . import DeviceProxy, TangoBasedRelease
from .emulator.mock_release import get_mock_device_proxy, get_mock_release


class Configuration(TypedDict):
    mock_release: Union[None, Mock]
    mock_device: Union[None, DeviceProxy]
    chart_subsystem_mapping: Dict[str, str]


def load_configuration() -> Configuration:
    if cfg_file_path := os.getenv("CONFIG_FILE"):
        with open(cfg_file_path, encoding="utf-8") as cf:
            config: dict = json.load(cf)
        chart_subsystem_mapping = config.get(
            "chart_subsystem_mapping", default_chart_subsystem_mapping()
        )
    else:
        chart_subsystem_mapping = default_chart_subsystem_mapping()

    if os.getenv("MOCKMODE"):
        return Configuration(
            mock_release=get_mock_release(),
            mock_device=get_mock_device_proxy(),
            chart_subsystem_mapping=chart_subsystem_mapping,
        )
    return Configuration(
        mock_release=None, mock_device=None, chart_subsystem_mapping=chart_subsystem_mapping
    )


def get_device_proxy(device_name: str) -> DeviceProxy:
    if proxy := configuration["mock_device"]:
        return proxy
    return cast(DeviceProxy, skallop_get_device_proxy(device_name))


def get_release() -> TangoBasedRelease:
    if release := configuration["mock_release"]:
        return release
    set_factory_provider(get_tango_factory())
    return get_mvp_release(chart_subsystem_mapping=configuration["chart_subsystem_mapping"])


def set_mock_release(mock_release: Mock):
    configuration["mock_release"] = mock_release


def set_mock_device(mock_device: DeviceProxy):
    configuration["mock_device"] = mock_device


def clear():
    configuration["mock_release"] = None
    configuration["mock_device"] = None


configuration: Configuration = load_configuration()
