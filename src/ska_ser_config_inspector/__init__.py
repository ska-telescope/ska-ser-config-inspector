from abc import abstractmethod

from ska_ser_skallop.connectors.configuration import AbstractDeviceProxy
from ska_ser_skallop.mvp_control.infra_mon.base import ResourceHealth
from ska_ser_skallop.mvp_control.infra_mon.configuration import DevicesChart, TangoBasedRelease
from ska_ser_skallop.mvp_control.infra_mon.helm import Chart
from ska_ser_skallop.mvp_control.infra_mon.tango_infra import Device


# we add additional methods we know exists on the real device proxy class
class DeviceProxy(AbstractDeviceProxy):
    @abstractmethod
    def get_attribute_list(self) -> list[str]:
        """Retrieve the list of available attributes."""


__all__ = [
    "DevicesChart",
    "TangoBasedRelease",
    "ResourceHealth",
    "DeviceProxy",
    "Chart",
    "Device",
]
