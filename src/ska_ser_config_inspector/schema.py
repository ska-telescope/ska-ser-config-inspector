from datetime import datetime
from typing import Any, Dict, List, Literal, Union

from pydantic import BaseModel

from . import ResourceHealth


class PingResponse(BaseModel):
    result: Literal["ok"]
    time: datetime


class DeploymentInstance(BaseModel):
    resource_type: Literal["Deployment", "Statefulset"]
    phase: Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]


class Deployments(BaseModel):
    overall_health: ResourceHealth
    instances: Dict[str, DeploymentInstance]


class Devices(BaseModel):
    overall_health: Literal["NOT_READY", "READY", "ERROR"]
    states: Dict[str, str]
    names: list[str]


class ChartResponse(BaseModel):
    chart: str
    version: str
    health: ResourceHealth
    deployments: Deployments
    devices: Union[None, Devices]

    def fullname(self) -> str:
        return f"{self.chart}-{self.version}"

    def __hash__(self):
        return hash(self.fullname())


class ChartSummary(BaseModel):
    chart: str
    version: str


class ReleaseResponse(BaseModel):
    chart: str
    version: str
    sub_charts: List[ChartSummary]


class DeviceResponse(BaseModel):
    name: str
    deployment_status: Literal["Running", "Pending", "Succeeded", "Failed", "Unknown"]
    device_properties: dict[str, Any]
    device_attributes: list[str]
    state: str = "Unknown"
    chart: str

    def fullname(self) -> str:
        return f"{self.chart}-{self.name}"

    def __hash__(self):
        return hash(self.fullname())


ChartQuery = Literal["health", "names", "device_states"]
