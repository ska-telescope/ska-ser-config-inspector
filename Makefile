# Chart for testing
# K8S_CHART = umbrella
# K8S_CHARTS = $(K8S_CHART)

## The following should be standard includes
# include core makefile targets for release management
-include .make/base.mk

KUBE_APP=config-inspector

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
# HELM_CHART ?= umbrella
HELM_CHARTS_TO_PUBLISH = ska-ser-config-inspector
# UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/

PYTHON_TEST_FILE ?= tests/unit
PYTHON_LINE_LENGTH := 99

TAG_PARAMS :=
DEPLOYMENT_PARAMS :=
ifneq ($(CI_JOB_ID),)
	DEV_TAG := $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
	OCI_BUILD_ADDITIONAL_TAGS = $(DEV_TAG)
	TAG_PARAMS = --set ska-ser-config-inspector.image.tag=$(DEV_TAG)
	DEPLOYMENT_PARAMS = --set global.minikube=false --set global.operator=true
endif

DEV_PARAMS :=
ifeq ($(DEVMODE),true)
	DEV_PARAMS = --set ska-ser-config-inspector.devmode=true
endif

K8S_CHART_PARAMS := --set global.labels.app=$(KUBE_APP) \
	$(K8S_EXTRA_PARAMS) \
	$(TAG_PARAMS) \
	$(DEV_PARAMS) \
	$(DEPLOYMENT_PARAMS)

# define private overrides for above variables in here
-include PrivateRules.mak

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#
include .make/k8s.mk
include .make/helm.mk
include .make/oci.mk
include .make/python.mk


.PHONY: help

run:
	@echo "###########################"
	$(info ${IMAGE_REPOSITORY})
	@echo "###########################"
	@make k8s-install-chart
	@make k8s-wait
	@echo
	@echo "##########################################################"
	@echo "# 	Docs page should be available at"
	@echo "#	http://${KUBE_HOST}/${KUBE_NAMESPACE}/config-inspector/docs"
	@echo "##########################################################"
	@open http://${KUBE_HOST}/${KUBE_NAMESPACE}/config-inspector/docs
.PHONY: run

local_redeploy: export K8S_CHART := umbrella
local_redeploy:
	eval $(minikube docker-env)
	kubectl config use-context minikube
	make oci-build-all
	make k8s-uninstall-chart
	make run
	minikube service --namespace $(KUBE_NAMESPACE) config-inspector --url 
.PHONY: local_redeploy

generate_schema:
	@poetry install
	@generate_schema openapi.json
.PHONY: generate_schema
